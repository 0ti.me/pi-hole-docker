#!/usr/bin/env bash

dt() {
  date --utc +%s
}

locked() {
  local EXPIRES=
  local LOCK=
  local LOCK_TIME_S=
  local MAX_LOCK_TIME_S=300
  local NOW=
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  NOW="$(dt)"
  LOCK="${THIS_DIR}/.lock"

  if [ -f "${LOCK}" ]; then
    LOCK_TIME_S="$(cat "${LOCK}")"
  else
    return 1
  fi

  let EXPIRES="${LOCK_TIME_S}"+"${MAX_LOCK_TIME_S}"

  if [ "${NOW}" -gt "${EXPIRES}" ]; then
    return 1
  else
    return 0
  fi
}

lock() {
  local LOCK=
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  LOCK="${THIS_DIR}/.lock"

  >&2 echo "Locking ${LOCK}"

  dt > "${LOCK}"
}

unlock() {
  local LOCK=
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  LOCK="${THIS_DIR}/.lock"

  >&2 echo "Unlocking ${LOCK}"

  rm -v "${LOCK}"
}

main() {
  local RESTART_FILE=${RESTART_FILE}
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  . "${THIS_DIR}/lib.sh"

  if locked; then
    >&2 echo "${THIS_DIR} is locked"

    return 0
  fi

  lock "${THIS_DIR}"

  2>/dev/null >&2 pushd "${THIS_DIR}"

  if ! [ -f "${RESTART_FILE}" ]; then
    RESTART_FILE="${THIS_DIR}/restart.sh"
  fi

  if lib-dc ps | any-unhealthy >/dev/null 2>&1; then
    if [ -f "${RESTART_FILE}" ]; then
      "${RESTART_FILE}" "$@"
    else
      lib-dc restart
    fi
  else
    >&2 echo "All containers healthy"
  fi

  2>/dev/null >&2 popd

  unlock "${THIS_DIR}"
}

main "$@"
