#!/usr/bin/env bash

any-unhealthy() {
  grep -E "(Exit )|(\(unhealthy\))"
}

lib-dc() {
  local COMPOSE_FILE=
  local THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  COMPOSE_FILE="${THIS_DIR}/docker-compose.yml"

  if [ -f "${COMPOSE_FILE}" ]; then
    docker-compose -f "${COMPOSE_FILE}" "$@"
  else
    >/dev/null 2>&1 pushd "${THIS_DIR}"

    docker-compose "$@"

    >/dev/null 2>&1 popd
  fi
}
